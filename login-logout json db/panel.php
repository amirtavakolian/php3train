<?php
session_start();
require "functions/functions.php";

if (isset($_COOKIE['user'])) {
 echo "<h1>Welcome " . $_SESSION['user'] . "</h1>";
 echo "<a href='?logout=true'>Logout</a>";
} else {
 $add = uri();
 header("location:$add");
}

if (isset($_GET['logout'])){
 unset($_COOKIE['user']);
 setcookie('user', null, -1, '/');
 $add = uri();
 header("location:$add");
}
