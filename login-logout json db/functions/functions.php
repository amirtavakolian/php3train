<?php

const ADDR = "http://127.0.0.1/oop";

// Define which page to be loaded:
function loadPage()
{
 if (isset($_GET['login'])) {
  require "template/3-sign-in.php";
 } else {
  require "template/2-sing-up.php";
 }
}

# Check username (not empty & more then 4 letters):
function checkUser($user)
{
 if (!empty($user) and (strlen($user) > 3)) {
  return true;
 } else {
  return false;
 }
}

# Check both password & re-password are same or not
function checkPass($pass, $re_pass)
{
 if (!empty($pass) and !empty($re_pass) and ($pass == $re_pass)) {
  return true;
 } else {
  return false;
 }
}

// Save sing-up form data in db:
function saveFormData($formData)
{
 $user = $formData['user'];
 $email = $formData['email'];

 $inp = file_get_contents('functions/db.json');
 $tempArray = json_decode($inp, true);

 // Check if user or email are not avilable:
 foreach ($tempArray as $key => $value) {
  if (($value['user'] == $user) || ($value['email'] == $email)) {
   return 0;
  }
 }

 // If uesr and email are not avialable:
 array_push($tempArray, $formData);
 $jsonData = json_encode($tempArray);
 file_put_contents('functions/db.json', $jsonData);
 header("location:?login=true");
}

// Login user
function loginUser($user, $pass)
{
 if (!empty($user) || (!empty($pass))) {
  $inp = file_get_contents('functions/db.json');
  $tempArray = json_decode($inp, true);
  foreach ($tempArray as $key => $value) {
   if (($value['user'] == $user) && ($value['pass'] == $pass)) {
    setcookie("user", "1", time() + 60 * 60 * 24 * 365, "/");
    $_SESSION['user'] = $user;
    header("location:panel.php");
   }
  }
 }
 return false;
}

function uri()
{
 return ADDR;
}
