<?php


# When sing-up form submited:
if (isset($_POST['signup'])) {
 $formData = $_POST;

 # Check username (not empty & more then 4 letters):
 $checkUser = checkUser($formData['user']);

 if ($checkUser) {
  # Check both password & re-password are same or not
  $checkPass = checkPass($formData['pass'], $formData['re_pass']);
 }

 # Save data in db ( json file )
 if (isset($checkPass) && ($checkPass)) {
  $saveFormResult = saveFormData($formData);
 }
}

?>

<body>
 <div class="main">
  <section class="signup">
   <div class="container">
    <div class="signup-content">
     <div class="signup-form">

      <?php if (isset($checkUser) and (!$checkUser)) {
       echo "<span style='color:red; display:inline-block; padding:10px; font-size:20px;'>user more then 3 letter or not empty</span>";
      }

      ?>
      <?php if (isset($checkPass) and (!$checkPass)) {
       echo "<span style='color:red; display:inline-block; padding:10px; font-size:20px;'>passwords must be same or not empty</span>";
      }
      ?>
      <?php if (isset($saveFormResult) and (!$saveFormResult)) {
       echo "<span style='color:red; display:inline-block; padding:10px; font-size:20px;'>Username or Email is available</span>";
      }

      ?>
      <a href="?login=true" class="signup-image-link" style="color:red; font-weight:700;">I am already member</a>

      <h2 class="form-title" style="margin-top:2rem;">Sign up</h2>

      <form method="POST" class="register-form" id="register-form">
       <div class="form-group">
        <label for="user"><i class="zmdi zmdi-account material-icons-name"></i></label>
        <input type="text" name="user" id="user" placeholder="Your Username" />
       </div>
       <div class="form-group">
        <label for="email"><i class="zmdi zmdi-email"></i></label>
        <input type="email" name="email" id="email" placeholder="Your Email" />
       </div>
       <div class="form-group">
        <label for="pass"><i class="zmdi zmdi-lock"></i></label>
        <input type="password" name="pass" id="pass" placeholder="Password" />
       </div>
       <div class="form-group">
        <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
        <input type="password" name="re_pass" id="re_pass" placeholder="Repeat your password" />
       </div>
       <div class="form-group">
        <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
        <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in <a href="#" class="term-service">Terms of service</a></label>
       </div>
       <div class="form-group form-button">
        <input type="submit" name="signup" id="signup" class="form-submit" value="Register" />
       </div>
      </form>
     </div>
     <div class="signup-image">
      <figure><img src="https://colorlib.com/etc/regform/colorlib-regform-7/images/signup-image.jpg" alt="sing up image"></figure>
     </div>
    </div>
   </div>
  </section>